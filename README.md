# XNAT 1.7 Selectable Table Plugin #

This is the XNAT 1.7 Selectable Table plugin. Its purpose is to overwrite standard non-dynamic tables in XNAT such as scan listings on an image session page and experiment listings on a subject page with an enhanced Selectable Table. The Selectable Table allows for the injection of inline per-scan actions, and actions that can be applied in bulk to multiple selected scans. 

This plugin was primarily developed for the Container Service to allow users to run scan-level commands, but can be used by other services as well. 

# Version 1.0 Release Notes #

* Overwrites the default XNAT scan listing for all image sessions that use the xnat-templates/screens/imageSessionData set of velocity template files. 
* Image session details now open in an XNAT dialog rather than in a hidden table row. Multiple dialogs can be opened side-by-side for scan comparison
* Users with appropriate edit permissions can edit scan notes in place
* Incorporates recent fixes in XNAT 1.7.3 for column display and snapshot rendering
* Fully integrated with Container Services 1.1. Users can run containers on individual scans, or in bulk on selected scans in the table. 

## Known Issues ##

* Integration of individual and bulk controls for downloading scans or opening specific scans in the viewer is not working. Rudimentary controls have been added to lead users to the session-level pages for both functions. Improvements are slated for the release of XNAT 1.7.5. 

# Building & Installing #

To build the XNAT 1.7 Selectable Table plugin:

1. If you haven't already, clone this repository and cd to the newly cloned folder.
1. Build the plugin: `./gradlew jar` (on Windows, you can use the batch file: `gradlew.bat jar`). This should build the plugin in the file **build/libs/xnat-selectable-table-1.0.0.jar** (the version may differ based on updates to the code).
1. Copy the plugin jar to your plugins folder: `cp build/libs/xnat-selectable-table-1.0.0.jar /data/xnat/home/plugins`
1. Restart Tomcat and your plugin will become active in XNAT. 


